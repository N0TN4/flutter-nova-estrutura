import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'controller.dart';

class PageInicial extends StatelessWidget {
  const PageInicial({
    @required this.titulo,
  });
  final String titulo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${titulo ?? ""}"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_right),
            onPressed: () {
              Navigator.pushNamed(context, '/segunda');
            },
          ),
        ],
      ),
      body: Consumer<PageInicialController>(
        builder: (_, controller, __) {
          if (controller.items != null) {
            return ListView.builder(
                itemCount: controller.items.length,
                itemBuilder: (context, index) {
                  return Text(controller.items[0].descricao ?? "");
                });
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

class Teste extends StatefulWidget {
  @override
  _TesteState createState() => _TesteState();
}

class _TesteState extends State<Teste> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
