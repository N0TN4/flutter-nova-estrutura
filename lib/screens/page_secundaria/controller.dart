import 'package:flutter/material.dart';
import 'package:rumoapp/models/Desenho.dart';
import 'package:rumoapp/services/Abstract_Service.dart';

class PageSecundariaController extends AbstractService<Desenho> {
  PageSecundariaController({@required String path}) : super(path) {
    this.findAll();
  }

  @override
  fromJson(json) => Desenho.fromJson(json);
}
