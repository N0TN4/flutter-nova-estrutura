import 'package:flutter/material.dart';
import 'package:rumoapp/models/Peca.dart';
import 'package:rumoapp/services/Abstract_Service.dart';

class PageTereciariaController extends AbstractService<Peca> {
  PageTereciariaController({@required String path}) : super(path) {
    this.findAll();
  }
  TextEditingController text1 = new TextEditingController();
  
  @override
  fromJson(json) => Peca.fromJson(json);
}
