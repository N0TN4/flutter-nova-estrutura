import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:rumoapp/components/card_item.dart';

import 'controller.dart';

class PageTereciaria extends StatelessWidget {
  final String path;
  const PageTereciaria({
    Key key,
    @required this.path,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PageTereciariaController(path: path),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Pagina 3"),
        ),
        body: Consumer<PageTereciariaController>(
          builder: (_, controller, __) {
            if (controller.items != null) {
              return TextField(
                controller: controller.text1,
              );
              // return ListView.builder(
              //     itemCount: controller.items.length,
              //     itemBuilder: (context, index) {

              //       return CardItem(
              //         title: controller.items[index].id.toString(),
              //         conteudo: controller.items[index].item,
              //         icon: Icons.add_photo_alternate,
              //       );
              //     });
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }
}
