// To parse this JSON data, do
//
//     final desenho = desenhoFromJson(jsonString);

import 'dart:convert';

Desenho desenhoFromJson(String str) => Desenho.fromJson(json.decode(str));

String desenhoToJson(Desenho data) => json.encode(data.toJson());

class Desenho {
  Desenho({
    this.id,
    this.descricao,
    this.imagem,
    this.pagina,
    this.obs,
    this.aplicacaoId,
    this.catalogoId,
  });

  int id;
  String descricao;
  String imagem;
  int pagina;
  String obs;
  int aplicacaoId;
  int catalogoId;

  factory Desenho.fromJson(Map<String, dynamic> json) => Desenho(
        id: json["id"],
        descricao: json["descricao"],
        imagem: json["imagem"],
        pagina: json["pagina"],
        obs: json["obs"],
        aplicacaoId: json["aplicacao_id"],
        catalogoId: json["catalogo_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "descricao": descricao,
        "imagem": imagem,
        "pagina": pagina,
        "obs": obs,
        "aplicacao_id": aplicacaoId,
        "catalogo_id": catalogoId,
      };
}
